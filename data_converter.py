import numpy as np
import scipy.io as tomat

data = np.load('train_data.npz')
in_data = data['in_data']

in_data_raw = []

for i in range(len(in_data)):
	for j in range(len(in_data[i])):
		in_data_raw.append(in_data[i][j])

train_in = np.reshape(in_data_raw,[len(in_data_raw),-1])

out_data = data['out_data']

out_data_raw = []

for i in range(len(out_data)):
	for j in range(len(out_data[i])):
		out_data_raw.append(out_data[i][j])

train_out = np.reshape(out_data_raw,[len(out_data_raw),-1])

data = np.load('test_data.npz')
in_data = data['in_data']

in_data_raw = []

for i in range(len(in_data)):
	for j in range(len(in_data[i])):
		in_data_raw.append(in_data[i][j])

test_in = np.reshape(in_data_raw,[len(in_data_raw),-1])

out_data = data['out_data']

out_data_raw = []

for i in range(len(out_data)):
	for j in range(len(out_data[i])):
		out_data_raw.append(out_data[i][j])

test_out = np.reshape(out_data_raw,[len(out_data_raw),-1])

tomat.savemat('dataset.mat',dict(train_in=train_in, train_out=train_out, test_in=test_in, test_out=test_out))